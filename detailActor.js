function LoadDetailActor(id) {

    LoadHTML();
    Loading();
    detailActor(id);
    console.log(id);
}
async function detailActor(id) {

    let request = `https://api.themoviedb.org/3/person/${id}?${api_key}&append_to_response=movie_credits`;
    const response = await fetch(request).catch(e=>{
        alert(`Caugth error: ${e.message}`);
        $('#loading').remove();
        return;
    });;
    const rs = await response.json();
    $('#loading').remove();
    console.log(rs);
    var now = new Date(rs.birthday);
    let date = now.getDate() + "/" + (now.getMonth() + 1) + "/" + now.getFullYear();

    $('#detail').empty();
    $('#detail').append(`
        <div class="col-md-7 pr-0">
            <img src="https://image.tmdb.org/t/p/w500/${rs.profile_path}" class="card-img" alt="...">
        </div>
        <div class="col-md-5">
            <div class="card pr-0">
                <div class="card-header" ><h5 class="text-center">${rs.name}</h5></div>
                <div class="card-body text-dark">
                    <div class="row">
                        <div class="col-3"> <p class="card-text"><b>Birthday:</b></p></div>
                        <div class="col-9"> <p class="card-text">${date}</p></div>
                    </div>
                    <p class="card-text">${rs.biography}</p>
                </div>
            </div>
        </div>

    `)
    listMovie(rs.movie_credits.cast);
}
async function listMovie(arrMovie) {
    Carousel('Movies');
    $('.carousel-inner').empty();
    let numGroupSlide = Math.ceil(arrMovie.length / 4);//
    for (i = 1; i <= numGroupSlide; i++) {
        $('.carousel-inner').append(`
        <div class="carousel-item ${i === 1 ? "active" : ""}">
            <div class="row item">

            </div>
        </div>
`);
    }
    let lenght = arrMovie.length;
    for (let i = 1; i <= numGroupSlide; i++) {
        for (let j = 0; j < 4; j++) {
            if ((i - 1) * 4 + j >= arrMovie.length) break;
            $(`.carousel-inner .carousel-item:nth-child(${i}) div.item`).append(`
        <div class="col-md-3 py-2">
            <div class="card bg-secondary h-100" onclick="LoadDetailMovie('${arrMovie[(i - 1) * 4 + j].id}')" >
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="https://image.tmdb.org/t/p/w500${arrMovie[(i - 1) * 4 + j].poster_path}" class="card-img" alt="...">
                    </div>
                    <div class="col-md-8 px-2" >
                        <h6 class="card-title">${arrMovie[(i - 1) * 4 + j].title === undefined ? arrMovie[(i - 1) * 4 + j].name : arrMovie[(i - 1) * 4 + j].title}</h6>
                        <div>
                            <p><b>Rate:</b>${arrMovie[(i - 1) * 4 + j].vote_average}</p>
                            <p class="m-0"><b>Charactor:</b>${arrMovie[(i - 1) * 4 + j].character}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         `);
        }
    }

}