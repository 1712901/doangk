const apiURL = 'https://api.themoviedb.org/3/';
const apiImg = 'https://image.tmdb.org/t/p/w500';
const api_key = 'api_key=6c05a01226ce529a4401676e4aeb9f91';
function evtSubmit(e) {
    e.preventDefault();
    // search theo people va Movie
    const query = $('form input').val();
    LoadHTMLHome();
    Search(query);
}
async function LoadPage(query,page=1) {
    
    const request = `${apiURL}trending/movie/day?${api_key}&page=${page}`;
    LoadHTMLHome();
    Loading();

    const response = await fetch(request).catch(e=>{
        alert(`Caugth error: ${e.message}`);
        $('#loading').remove();
        return;
    });
    const rs = await response.json();
    $('#loading').remove();

    for (const movie of rs.results) {
        fillMovie(movie);   
    }
    Pagination('LoadPage',query,page);
}
function fillMovie(movie) {
    $('#listMovie').append(`
    <div class="col-md-4 py-2">
        <div class="card h-100" onclick="LoadDetailMovie('${movie.id}')" >
            <div class="row no-gutters">
                <div class="col-md-4">
                    <img src="${apiImg}${movie.poster_path}" class="card-img" alt="...">
                </div>
                <div class="col-md-8" >
                    <div class="card-body overflow-hidden" style="padding-bottom:0px">
                        <h5 class="card-title">${movie.title === undefined ? movie.name : movie.title}</h5>
                        <div class="overflow-hidden">
                            <p class="m-0"><b>Rate:</b>${movie.vote_average}</p>
                            <p class="card-text" style="height: 100px;">${movie.overview}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
`)
}
function Loading() {
    $('.container-fuild').append(`
    <div id="loading" class="d-flex justify-content-center ">
        <div class="spinner-grow text-light m-5" style="width: 10rem; height: 10rem;" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    `)
}
function Pagination(funcName,query,page){
    $('.container-fuild').append(` 
    <div class="row">
        <nav class="col-md" aria-label="Page navigation example">
            <ul class="pagination justify-content-center" >
            
            </ul>
        </nav>
    </div>
        `);

    $('.pagination li').remove();
    $('.pagination').append(`
            <li class="page-item" onclick="${funcName}('${query}',${page-1})">
                        <a class="page-link"  aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
             </li>
            <li class="page-item" onclick="${funcName}('${query}',${page+1})">
                    <a class="page-link"  aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
            </li>
    `)
    let first=page;
    if(page%3===0){
        first=page-2;
    }else{
        first=parseInt(page/3)*3+1;
    }
    for(let i=first;i<=first+2;i++){
        $('.pagination li:last-child').before(`
            <li class="page-item ${i===page?"active":""}" onclick="${funcName}('${query}',${i})"><a class="page-link" >${i}</a></li>
        `); 
    }

}
async function Search(query,page=1) {
    LoadHTMLHome(); 
    Loading();

    searchMovieAndPeople(query,page);

    Pagination('Search',query,page);
}

async function searchMovieAndPeople(query,page=1){

    const request =`${apiURL}search/multi?${api_key}&query=${query}&page=${page}`;
    const response = await fetch(request).catch(e=>{
        alert(`Caugth error: ${e.message}`);
        $('#listMovie #loading').empty();
        return;
    });
    const rs = await response.json();
    $('#loading').remove();
    console.log(rs.results);
    
    for (const rsMovie of rs.results) {
        if(rsMovie.media_type==="movie"){
            fillMovie(rsMovie);
        }else if(rsMovie.media_type==="person"&&rsMovie.known_for_department=="Acting"){
            for (const rsPeople of rsMovie.known_for) {
                fillMovie(rsPeople);
            }
        }
    }
}
function LoadHTMLHome(){

    $('.container-fuild div:not(:first-child)').remove();
    $('.container-fuild').append(`
    <div class="row" id="listMovie">
            <!-- <div class="col-md-4 py-2">
                <div class="card shadow">
                    <img src="#" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title">Card title</h5>
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of
                            the
                            card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div> -->
    </div>
    `);
}

