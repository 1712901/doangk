
const UrlgetMovie = 'https://api.themoviedb.org/3/movie/';
function LoadDetailMovie(id) {
    LoadHTML();
    Loading();
    detailMovie(id);
    console.log(id);
}
function LoadHTML() {
    $('.container-fuild div:not(:first-child)').remove();

    $('.container-fuild').append(`
    <div class="container">
    <div class="row py-2" id="detail">
        <div class="col-md-4">

        </div>
        <div class="col-md-8">

        </div>
    </div>
   
    </div>
    `);
}
async function detailMovie(id) {
    let request = `${UrlgetMovie}${id}?${api_key}&append_to_response=credits,reviews`;
    const response = await fetch(request).catch(e=>{
        alert(`Caugth error: ${e.message}`);
        $('#loading').remove();
        return;
    });
    const rs = await response.json();

    $('#loading').remove();
    console.log(rs);
    let listgenre = "";
    for (const genre of rs.genres) {
        listgenre += (genre.name + ",");
    }
    var now = new Date(rs.release_date);
    let date = now.getDate() + "/" + (now.getMonth() + 1) + "/" + now.getFullYear();
    let nameDirector = "";
    for (const crew of rs.credits.crew) {
        if (crew.job === "Director") {
            nameDirector += crew.name+",";
            break;
        }
    }
    $('.container-fuild .container').empty();
    $('.container-fuild .container').append(`
        <div class="row py-2" id="detail" style="background-image:url(https://image.tmdb.org/t/p/original${rs.backdrop_path}); background-repeat: no-repeat;background-size:100% 100%;">
            <div class="col-md-5 pr-0 mt-5" >
                <img src="https://image.tmdb.org/t/p/w500/${rs.poster_path}" class="card-img" alt="...">
            </div>
            <div class="col-md-7 text-info">
                <div class="card pr-0" style="background:none;border:none;">
                    <div class="card-header" ><h5 class="text-center">${rs.title}</h5></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-3"> <p class="card-text"><b>Year:</b></p></div>
                            <div class="col-9"> <p class="card-text">${date}</p></div>
                        </div>
                        
                        <div class="row">
                            <div class="col-3"> <p class="card-text"><b>Director:</b></p></div>
                            <div class="col-9"> <p class="card-text">${nameDirector}</p></div>
                        </div>
                        
                        <div class="row">
                            <div class="col-3"> <p class="card-text"><b>Genres:</b></p></div>
                            <div class="col-9"> <p class="card-text">${listgenre}</p></div>
                        </div>
                       
                        <div class="row">
                            <div class="col-3">  <p class="card-text"><b>Rate:</b></p></div>
                            <div class="col-9"> <p class="card-text">${rs.vote_average}</p></div>
                        </div>
                        <div class="row">
                            <div class="col-3">  <p class="card-text"><b>Lenght:</b></p></div>
                            <div class="col-9"> <p class="card-text">${rs.runtime} minius</p></div>
                        </div>
                        <p class="card-text"><b>Overview:</b>${rs.overview}</p>
                    </div>
                </div>
            </div>
        </div>
    `);
    listActor(rs.credits.cast);
    getReview(rs.reviews.results);
}
async function listActor(arrActor) {
    Carousel('Actors');
    let numGroupSlide = Math.ceil(arrActor.length / 6);//
    for (i = 1; i <= numGroupSlide; i++) {
        $('.carousel-inner').append(`
        <div class="carousel-item ${i === 1 ? "active" : ""}">
            <div class="row">
            </div>
        </div>
    `);
    }
    for (let i = 1; i <= numGroupSlide; i++) {
        for (let j = 0; j < 6; j++) {
            if ((i - 1) * 6 + j >= arrActor.length) break;
            $(`.carousel-inner .carousel-item:nth-child(${i}) div.row`).append(`
                            <div class="col-sm-2 px-1 d-flex"onclick="LoadDetailActor('${arrActor[(i-1)*6+j].id}')">
                                <img src="${arrActor[(i - 1) * 6 + j].profile_path === null ? './img/icon.png' : `https://image.tmdb.org/t/p/w500/${arrActor[(i - 1) * 6 + j].profile_path}`}"
                                    class="w-100 align-items-center" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>${arrActor[(i - 1) * 6 + j].name}</h5>
                                </div>
                             </div>
             `);
        }
    }

}
function getReview(reviews) {
    $('.container-fuild .container').append(`
    <div class="row reviews py-2">
        <div class="w-100 bg-light">
            <h3 class="d-inline m-0">Comment</h3>
            <h6 class="d-inline m-0">(${reviews.length})</h6>
        </div>
    </div>
    `);
    for (const review of reviews) {
        $('.container-fuild .reviews').append(`
        <div class="col-md-12 card p-0">
            <div class="card-body p-2">
                <h6 class="card-subtitle text-muted">${review.author}</h6>
                <div class="text-truncate ">
                    <p class="card-text m-0 d-inline">${review.content}</p>
                </div>
                <a href="${review.url}" class=" card-link" target="_blank">Seemore</a>
            </div>
        </div>
    `);
    }

}
function Carousel(list) {
    $('.container-fuild .container').append(`
        <div class="row py-2">
            <h3 class="bg-light w-100">${list}</h3>
            <div class="bd-example">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">

                </div>
                <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev"
                    style="width:25px">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next"
                    style="width:25px">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                </div>
            </div>
        </div>
        `);
}